from django.shortcuts import render


def home(request):
    return render(request, 'main/home.html')

def edu(request):
    return render(request, 'main/edu.html')

def profil(request):
    return render(request, 'main/profil.html')
